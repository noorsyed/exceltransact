﻿/* Common app functionality */

var app = (function () {
    "use strict";

    var app = {};

    // Common initialization function (to be called from each page)
    app.initialize = function () {
        $('body').append(
            '<div id="notification-message">' +
                '<div class="padding">' +
                    '<div id="notification-message-close"></div>' +
                    '<div id="notification-message-header"></div>' +
                    '<div id="notification-message-body"></div>' +
                '</div>' +
            '</div>');

        $('#notification-message-close').click(function () {
            $('#notification-message').hide();
        });


        // After initialization, expose a common notification function
        app.showNotification = function (header, text, hasError) {
            $('#notification-message-header').text(header);
            $('#notification-message-body').text(text);
            $('#notification-message').slideDown('fast');
            if (hasError) {
                if (header == 'error') {
                    $('#notification-message').css('background', '#990000')
                    $('#notification-message-body').css('width', '140px')
                }
                else
                {
                    $('#notification-message').css('background', '#990000')
                    $('#notification-message-body').css('width', '100%')
                }
            }
            else
            {
                $('#notification-message').css('background', '#666666')
                $('#notification-message-body').css('width', '100%')
            }
        };
    };

    return app;
})();