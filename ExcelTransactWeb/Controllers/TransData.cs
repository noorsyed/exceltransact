﻿namespace ExcelTransactWeb.Controllers
{
    public class TransData
    {
        public string[,] Rows { get; set; }
        public bool HasRowId { get; set; }
        public bool HasColumnHeader { get; set; }
        public short ValueColumns { get; set; }
        public short KeyColumns { get; set; }
        public string Selection { get; set; }
        public string ChartSelection { get; set; }
        public string SheetName { get; set; }
        public int SheetNo { get; set; }
    }
}