﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ExcelTransactWeb;
using System.Web.Http.Cors;
using System.Web;

namespace ExcelTransactWeb.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TransactionController : ApiController
    {
        private ADDINEntities db = new ADDINEntities();
        private string[] Columns = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P",
            "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF","AG","AH","AI" };

        // GET: api/Transaction
        public IQueryable<TRANSACTION> GetTRANSACTIONS()
        {
            return db.TRANSACTIONS;
        }

        // GET: api/Transaction/5
        [ResponseType(typeof(TRANSACTION))]
        public async Task<IHttpActionResult> GetTRANSACTION(long id)
        {
            TRANSACTION tRANSACTION = await db.TRANSACTIONS.FindAsync(id);
            if (tRANSACTION == null)
            {
                return NotFound();
            }

            return Ok(tRANSACTION);
        }

        // PUT: api/Transaction/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTRANSACTION(long id, TRANSACTION tRANSACTION)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tRANSACTION.TRANSACTION_ID)
            {
                return BadRequest();
            }

            db.Entry(tRANSACTION).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TRANSACTIONExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Transaction

        public async Task<TransData> PostTRANSACTION([FromBody]TransData data)
        {
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            //db.TRANSACTIONS.Add(tRANSACTION);


            int r = data.HasRowId ? 1 : 0;
            int c = data.HasColumnHeader ? 1 : 0;
            int rowCnt = 0;
            var startDateTime = DateTime.Now;
            try
            {
                int sheetNo = data.SheetNo + 1;
                string hostName = Dns.GetHostName();
                string myIP = Dns.GetHostEntry(hostName).AddressList[0].ToString();

                var trans = new TRANSACTION();
                trans.IP_ADDRESS = HttpContext.Current.Request.UserHostAddress;
                trans.RECORDS_COUNT = data.Rows.GetLength(0);
                trans.TIMESTAMP = DateTime.Now;
                trans.ROW_ID_CHECK = data.HasRowId;
                trans.HEADERS_CHECK = data.HasColumnHeader;
                trans.VALUECOLUMNS_COUNT = data.ValueColumns;
                trans.KEYCOLUMNS_COUNT = data.KeyColumns;
                trans.START_SAVEDATA_TIME = startDateTime;

                trans.USERNAME = "Admin";
                db.TRANSACTIONS.Add(trans);
                await db.SaveChangesAsync();


               
                string[,] offsetArray = new string[data.Rows.GetLength(0), 2];
                for (int i = c; i < data.Rows.GetLength(0); i++)
                {
                    var transData = new TRANSACTIONS_DATA
                    {

                    };

                    for (int j = r; j < data.Rows.GetLength(1); j++)
                    {

                        if (r == 1)
                        {
                            //transData.ROW_ID = Convert.ToInt32(data.Rows[i, 0]);
                            transData.ROW_ID = data.Rows[i, 0];
                        }
                        SetTransData(data, j - r, i, j, transData);
                    }

                    transData.TRANSACTION_ID = trans.TRANSACTION_ID;  //0;//TODO: set Transaction primary key



                    SetOffsetArray(trans, offsetArray,c);

                    db.TRANSACTIONS_DATA.Add(transData);
                }

                //var transData = new TRANSACTIONS_DATA
                //{

                //};

                //int cnt = 0;
                //cnt = r+c;

                //for (int i = r; i < data.Rows.GetLength(0); i++)
                //{


                //    for (int j = c; j < data.Rows.GetLength(1); j++)
                //    {

                //        if (r == 1)
                //        {
                //            transData.ROW_ID = Convert.ToInt32(data.Rows[i, 0]);
                //        }
                //        SetTransData(data, cnt++, i, j, transData);
                //    }


                //}

                //transData.TRANSACTION_ID = trans.TRANSACTION_ID;//0;//TODO: set Transaction primary key
                //db.TRANSACTIONS_DATA.Add(transData);



                await db.SaveChangesAsync();


                //Updating finishtime

                var transDetails = db.TRANSACTIONS.Where(x => x.TRANSACTION_ID == trans.TRANSACTION_ID).FirstOrDefault();
                var finishDateTime = DateTime.Now;
                transDetails.FINISH_SAVETIME_TIME = finishDateTime;
                await db.SaveChangesAsync();

                var columnOffset = 2;// + r;



                //TODO : columnOffset reset
                // columnOffset = 0;

                data.SheetName = "NewSheet" +sheetNo;// new Random().Next(1,10);
                data.SheetNo = sheetNo;
                data.Selection = $"{data.SheetName}!A1:{Columns[data.Rows.GetLength(1) - 1 + columnOffset]}{ data.Rows.GetLength(0)}";
                data.ChartSelection = $"{data.SheetName}!C1:{Columns[data.Rows.GetLength(1) - 1 + columnOffset]}{ data.Rows.GetLength(0)}";
                string[,] result = new string[data.Rows.GetLength(0), data.Rows.GetLength(1) + 2];

                UnionOffsetAndResult(data, offsetArray, result);
                data.Rows = result;
                await db.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;

            }
            return await Task.FromResult<TransData>(data);
            // return CreatedAtRoute("DefaultApi", new { id = tRANSACTION.TRANSACTION_ID }, tRANSACTION);
        }

        private static void UnionOffsetAndResult(TransData data, string[,] offsetArray, string[,] result)
        {
            for (int i = 0; i < result.GetLength(0); i++)
            {
                for (int j = 0; j < result.GetLength(1); j++)
                {
                    if (j < 2)
                        result[i, j] = offsetArray[i, j];
                    else
                        result[i, j] = data.Rows[i, j - 2];

                }
            }
        }

        private static void SetOffsetArray(TRANSACTION trans, string[,] offsetArray,int hasColumn)
        {
            for (int p = 0; p < offsetArray.GetLength(0); p++)
            {
                for (int q = 0; q < 2; q++)
                {
                    if (p == 0 && hasColumn==1)
                    {
                        if (q == 0)
                        {
                            offsetArray[p, q] = "TRANSACTION_ID";
                        }
                        else
                        {
                            offsetArray[p, q] ="USERNAME";
                        }
                    }
                    else
                    {
                        if (q == 0)
                        {
                            offsetArray[p, q] = trans.TRANSACTION_ID.ToString();
                        }
                        else
                        {
                            offsetArray[p, q] = trans.USERNAME;
                        }
                    }
                }
            }
        }

        private static void SetTransData(TransData data, int r, int i, int j, TRANSACTIONS_DATA transData)
        {
            switch (r)
            {
                case 0:
                    transData.FIELD01 = data.Rows[i, j];
                    break;
                case 1:
                    transData.FIELD02 = data.Rows[i, j];
                    break;
                case 2:
                    transData.FIELD03 = data.Rows[i, j];
                    break;
                case 3:
                    transData.FIELD04 = data.Rows[i, j];
                    break;
                case 4:
                    transData.FIELD05 = data.Rows[i, j];
                    break;
                case 5:
                    transData.FIELD06 = data.Rows[i, j];
                    break;
                case 6:
                    transData.FIELD07 = data.Rows[i, j];
                    break;
                case 7:
                    transData.FIELD08 = data.Rows[i, j];
                    break;
                case 8:
                    transData.FIELD09 = data.Rows[i, j];
                    break;
                case 9:
                    transData.FIELD10 = data.Rows[i, j];
                    break;
                case 10:
                    transData.FIELD11 = data.Rows[i, j];
                    break;
                case 11:
                    transData.FIELD12 = data.Rows[i, j];
                    break;
                case 12:
                    transData.FIELD13 = data.Rows[i, j];
                    break;
                case 13:
                    transData.FIELD14 = data.Rows[i, j];
                    break;
                case 14:
                    transData.FIELD15 = data.Rows[i, j];
                    break;
                case 15:
                    transData.FIELD16 = data.Rows[i, j];
                    break;
                case 16:
                    transData.FIELD17 = data.Rows[i, j];
                    break;
                case 17:
                    transData.FIELD18 = data.Rows[i, j];
                    break;
                case 18:
                    transData.FIELD19 = data.Rows[i, j];
                    break;
                case 19:
                    transData.FIELD20 = data.Rows[i, j];
                    break;
                case 20:
                    transData.FIELD21 = data.Rows[i, j];
                    break;
                case 21:
                    transData.FIELD22 = data.Rows[i, j];
                    break;
                case 22:
                    transData.FIELD23 = data.Rows[i, j];
                    break;
                case 23:
                    transData.FIELD24 = data.Rows[i, j];
                    break;
                case 24:
                    transData.FIELD25 = data.Rows[i, j];
                    break;

                case 25:
                    transData.FIELD26 = data.Rows[i, j];
                    break;
                case 26:
                    transData.FIELD27 = data.Rows[i, j];
                    break;
                case 27:
                    transData.FIELD28 = data.Rows[i, j];
                    break;
                case 28:
                    transData.FIELD29 = data.Rows[i, j];
                    break;
                case 29:
                    transData.FIELD30 = data.Rows[i, j];
                    break;


                default:
                    break;
            }
        }

        // DELETE: api/Transaction/5
        [ResponseType(typeof(TRANSACTION))]
        public async Task<IHttpActionResult> DeleteTRANSACTION(long id)
        {
            TRANSACTION tRANSACTION = await db.TRANSACTIONS.FindAsync(id);
            if (tRANSACTION == null)
            {
                return NotFound();
            }

            db.TRANSACTIONS.Remove(tRANSACTION);
            await db.SaveChangesAsync();

            return Ok(tRANSACTION);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TRANSACTIONExists(long id)
        {
            return db.TRANSACTIONS.Count(e => e.TRANSACTION_ID == id) > 0;
        }
    }
}