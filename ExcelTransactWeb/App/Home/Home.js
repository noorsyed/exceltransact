﻿//define ng app
var myapp = angular.module("ExcelTransact", ['ngMaterial', 'ngAnimate', 'treasure-overlay-spinner'])

    .controller("mainCtrl", ["$scope", "$q", "excelService", "$http", "$mdToast",
function mainCtrl($scope, $q, excelService, $http, $mdToast) {

    "use strict";

    $scope.cells = [];
    $scope.data = {};
    $scope.spinner = { active: false };
    $scope.submitted = false;
    $scope.sheetNo = 1;

    var ctx = new Excel.RequestContext();

    // Excel 2013 get selected cell value
    $scope.getDataFromSelection = excelService.getDataFromSelection;


    $scope.getRange = function (field) {

        var selectedRange = ctx.workbook.getSelectedRange();
        selectedRange.load("address")
        ctx.sync().then(function () {
            $scope.data[field] = selectedRange.address;

            //excelService.getRangeValues(selectedRange.address).then(function (rows) {
            //    if (rows.length > 0) {
            //        var totalColumns = rows[0].length;
            //        if ($scope.data.hasRowId) {
            //            $scope.data.keyColumns = $scope.data.valueColumns = (totalColumns - 1) / 2;
            //        }
            //        else {
            //            $scope.data.keyColumns = $scope.data.valueColumns = totalColumns / 2;
            //        }
            //    }
            //}).catch(function (err) {
            //    console.log(err);
            //}).finally(function () {
            //    console.log("done");
            //});

        })
    }


    $scope.clear = function () {
        $scope.data = {};
        $scope.data.keyColumns = 0;
        $scope.data.valueColumns = 0;

        $scope.isdisable = true;
    }


    $scope.save = function (excelTransForm) {
  var validationSummary = [];
        if (excelTransForm.$valid) {
            $scope.isSubmit = true;
            excelService.getRangeValues($scope.data.range).then(function (rows) {
                var actualColumns = 0;
                if (rows.length > 0) {
                    var totalColumns = rows[0].length;
                    if ($scope.data.hasRowId) {
                        actualColumns = $scope.data.valueColumns + $scope.data.keyColumns + 1;
                    }
                    else {
                        actualColumns = $scope.data.valueColumns + $scope.data.keyColumns;
                    }
                    if (totalColumns == actualColumns) {
                        proceedWithSave();
                    }
                    else {
                        app.showNotification("warning", "invalid entries, please cross check", true);
                    }
                }
            }).catch(function (err) {
                app.showNotification("error", err.data.exceptionMessage || err.data.message, true);
            }).finally(function () {

            });


        }

        else {
          
            excelTransForm.$error.required.forEach(function (err) {
                validationSummary.push(err.$name+' required');
            });
            app.showNotification("error", validationSummary.join('\r\n'), true);

        }
    };


    function proceedWithSave() {

        $scope.spinner = { active: true };
        var selectedRange = ctx.workbook.getSelectedRange();
        selectedRange.load("address")
        ctx.sync().then(function () {
            excelService.getRangeValues(selectedRange.address)
                .then(function (rows) {
                    $http({
                        method: "POST",
                        url: "https://localhost:44300/api/transaction",
                        data: {
                            rows: rows,
                            valueColumns: $scope.data.valueColumns,
                            keyColumns: $scope.data.keyColumns,
                            hasRowId: $scope.data.hasRowId,
                            hasColumnHeader: $scope.data.hasColumnHeader,
                            selection: selectedRange.address,
                            sheetNo: $scope.sheetNo
                        }
                    })
                        .then(function (res) {

                            excelService.setRangeValues(res.data.selection, res.data.rows, res.data.sheetName, res.data.chartSelection).then(function (success) {
                                console.log(success);
                                $scope.sheetNo = res.data.sheetNo;
                                $scope.spinner = { active: false };
                                $scope.submitted = true;
                                $scope.data = {};
                                $scope.isdisable = true;
                                app.showNotification("Save", "Data submitted successfully");

                            }, function (err) {
                                console.log(err);
                                app.showNotification("error", err.data.exceptionMessage || err.data.message, true);
                                $scope.spinner = { active: false };
                            })
                        }, function (error) {
                            console.log(error);
                            app.showNotification("error", error.data.exceptionMessage || error.data.message, true);
                            $scope.spinner = { active: false };

                        });


                });

        });
    }


    // works only in Excel 2016
    $scope.copyCells = function () {

        excelService.getRangeValues("A1:B2").then(function (rows) {
            return excelService.setRangeValues("A4:B5", rows);
        }).then(function (data) {
            console.log("success");
        }).catch(function (err) {
            console.log(err);
        }).finally(function () {
            console.log("done");
        });

    };


    $scope.getColumnLetter = function (n) {
        switch (n) {
            case 0:
                return "A";
            case 1:
                return "B";
            case 2:
                return "C";
            case 3:
                return "D";
            default:
                return n;
        }
    };
}])




.factory("excelService", ["$q", function ($q) {

    "use strict";

    app.initialize();



    // Reads data from current document selection and displays a notification
    function getDataFromSelection() {
        Office.context.document.getSelectedDataAsync(Office.CoercionType.Text,
            function (result) {
                if (result.status === Office.AsyncResultStatus.Succeeded) {
                    //app.showNotification('The selected text is:', '"' + result.value + '"');
                    console.log('The selected text is:', '"' + result.value + '"');
                } else {
                    //app.showNotification('Error:', result.error.message);
                    console.log('Error:', result.error.message);
                }
            }
        );
    }




    return {
        getDataFromSelection: getDataFromSelection,
        getRangeValues: function (selector) {
            var deferred = $q.defer();
            var data = null;
            Excel.run(function (ctx) {
                var range = ctx.workbook.worksheets.getActiveWorksheet().getRange(selector);
                range.load("address, values, range/format");
                return ctx.sync().then(function () {
                    data = range.values;
                });
            }).then(function () {
                deferred.resolve(data);
            }).catch(function (error) {
                deferrred.reject(error.message);
            });
            return deferred.promise
        },
        setRangeValues: function (selector, data, sheetName, chartSelector) {
            var deferred = $q.defer();
            Excel.run(function (ctx) {

                var worksheet = ctx.workbook.worksheets.add(sheetName);
                worksheet.activate();
                worksheet.load('name');
                return ctx.sync().then(function () {
                    console.log(worksheet.name);
                    var range = ctx.workbook.worksheets.getActiveWorksheet().getRange(selector);
                    range.load("address, values, range/format");
                    return ctx.sync().then(function () {
                        range.values = data;

                        var chartRange = ctx.workbook.worksheets.getActiveWorksheet().getRange(chartSelector);
                        chartRange.load("range/format");
                        return ctx.sync().then(function () {

                            var chart = worksheet.charts.add("LineMarkersStacked", chartRange, "auto");
                            return ctx.sync().then(function () {

                                deferred.resolve(data);
                            }).catch(function (error) {
                                console.log("Error: " + error);
                                deferrred.reject(error);
                                if (error instanceof OfficeExtension.Error) {
                                    console.log("Debug info: " + JSON.stringify(error.debugInfo));
                                }
                            });

                        });




                    });
                });
            }).then(function () {
                deferred.resolve(data);
            }).catch(function (error) {
                deferrred.reject(error.message);
            });
            return deferred.promise
        }
    };

}]);

//manually initialize ng app when office.js is ready
Office.initialize = function (reason) {
    angular.element(document).ready(function () {
        angular.bootstrap(document, ["ExcelTransact"]);
    });
};






